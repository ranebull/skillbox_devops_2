# Package application in Docker and deploy it to Kubernetes

Wrap the project [flatris](https://github.com/timurb/flatris) in the Docker (using Dockerfile and Docker Compose)
I use the next best practises for writing Dockerfile and Docker Compose file:

* Linting ( for Dockerfile - [fromlatest.io](https://www.fromlatest.io/#/) and [hadolint](https://hadolint.github.io/hadolint/) ; for Markdown - [markdownlint demo
](https://dlaa.me/markdownlint/))
* `.dockerignore` file - for excluding some files from container filesystem
* Log rotation for docker container
* exec form for `ENTRYPOINT` instruction

Using Gitlab CI the stages of build, test and deployment to Kubernetes (I used Yandex.Cloud) were described.

Useful links:
https://docs.docker.com/engine/reference/builder/#dockerignore-file
https://docs.docker.com/config/containers/logging/configure/
https://classic.yarnpkg.com/en/docs/cli/test/
https://docs.gitlab.com/ee/ci/docker/using_docker_images.html#overriding-the-entrypoint-of-an-image
https://cloud.yandex.com/docs/solutions/infrastructure-management/gitlab-containers
https://cloud.yandex.ru/docs/solutions/infrastructure-management/gitlab-containers
