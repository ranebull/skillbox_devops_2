FROM node:lts

WORKDIR /app

COPY package.json /app

RUN yarn install

COPY . /app
RUN chmod a+x ./scripts/test

RUN yarn build

EXPOSE 3000

ENTRYPOINT ["yarn", "start"]
